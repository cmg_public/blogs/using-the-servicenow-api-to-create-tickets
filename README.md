# Using The ServiceNow API To Create Tickets

## Overview

This repository has one Python file (main.py) that outlines how to create multiple ticket types in ServiceNow. While Python is used in this project, the same process can be used in other languages as the API endpoints are the same regardless. If you would like to use any of the code in this repository, you can either clone it or just copy the parts you want.

There is a blog article that goes along with this repository, which can be found [here](https://www.515tech.com/post/using-the-servicenow-api-to-create-tickets).