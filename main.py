import requests
import json
import os
from dotenv import load_dotenv

load_dotenv()

base_url = os.environ.get('SNOW_BASE_URL')
username = os.environ.get('SNOW_USERNAME')
password = os.environ.get('SNOW_PASSWORD')
headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
}  # Define headers for ServiceNow API requests
verify = False



# Create Incident

payload = {
    'short_description': 'This is a short description',
    'summary': 'This is a summary'
}  # Define the payload for creating the incident
table_name = 'incident'  # Define the table name that will be modified

try:
    url = f'{base_url}/api/now/table/{table_name}'  # Define the URL for the request
    response = requests.post(url, auth=(username, password), headers=headers, data=json.dumps(payload),
                             verify=verify)  # Add the item to the ServiceNow table

    if response.status_code == 201:  # If the item was successfully added to the table
        print(json.dumps(response.json(), indent=4))
    else:
        print(f'There was a problem adding an entry to the ServiceNow table "{table_name}". '
        f'{response.text} : {response.status_code}')
except Exception as e:
    print(e)



# Create Change Request

payload = {
    'short_description': 'This is a short description',
    'summary': 'This is a summary'
}  # Define the payload for creating the change request

try:
    url = f'{base_url}/api/sn_chg_rest/change/normal'  # Define the URL for the request
    response = requests.post(url, auth=(username, password), headers=headers, data=json.dumps(payload),
                             verify=verify)  # Create the ServiceNow Change Request

    if response.status_code == 200:  # If the change was successfully created
        print(json.dumps(response.json(), indent=4))
    else:
        print(f'There was a problem creating the ServiceNow Change Request. '
        f'{response.text} : {response.status_code}')
except Exception as e:
    print(e)



# Create Service Catalog Request

payload = {
    'sysparm_quantity': 1,
    'variables': {
    }
}  # Define the payload for creating the request
sc_item_name = 'Standard Laptop'  # Define the Service Catalog item name

try:
    url = f'{base_url}/api/sn_sc/servicecatalog/items'  # Add them to the URL
    response = requests.get(url, auth=(username, password), headers=headers,
                            verify=verify)  # Get the Service Catalog item(s)

    if response.status_code == 200:  # If the data was successfully retrieved
        items = {x['name']: x['sys_id'] for x in response.json()['result']}  # Create dictionary mapping of service catalog item to sys_id

        if sc_item_name in items.keys():  # If the item was found
            sc_req_item_id = items[sc_item_name]  # Get the system ID of the item
            url = f'{base_url}/api/sn_sc/servicecatalog/items/{sc_req_item_id}/order_now'  # Define the URL for the request

            response = requests.post(url, auth=(username, password), headers=headers, data=json.dumps(payload),
                                     verify=verify)  # Create the request

            if response.status_code == 200:  # If the item was successfully created
                print(json.dumps(response.json(), indent=4))
            else:
                print(f'There was a problem creating the ServiceNow Request. '
                      f'{response.text} : {response.status_code}')
    else:
        print(f'There was a problem retrieving the ServiceNow catalog items. '
              f'{response.text} : {response.status_code}')
except Exception as e:
    print(e)


